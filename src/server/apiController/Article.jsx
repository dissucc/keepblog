import redisClient, { InitRedis } from '../utils/redisClient';
import { redisConfig } from '../config';

export default {
    controllerName: 'article',
    todoList: [
        {
            name: 'addarticle',
            todo: (req, callback) => {
                let result = new ResultClass({ rel: false, msg: '操作失败！', code: '-1' });
                const entity = req.body;
                const client = InitRedis(redisConfig);
                redisClient.getString(client, 'articleindex')
                    .then((res) => {
                        if (res == null) {
                            redisClient.setString(client, 'articleindex', 1);
                            return 1;
                        }
                        else {
                            redisClient.setString(client, 'articleindex', parseInt(res) + 1);
                            return parseInt(res) + 1;
                        }
                    })
                    .then((key) => {
                        redisClient.setHashValue(client, 'article', key, JSON.stringify(entity))
                            .then((res) => {
                                if (res) {
                                    result.rel = true;
                                    result.msg = "保存成功！";
                                    result.code = 10000;
                                }
                                if (callback) {
                                    callback(result);
                                }
                            });
                    });
            }
        },
        {
            name: 'getarticle',
            todo: (req, callback) => {
                let result = new ResultClass({ rel: false, msg: '操作失败！', code: '-1' });
                const client = InitRedis(redisConfig);
                redisClient.getHashValueAll(client, 'article').then((res) => {

                    result.rel = true;
                    result.msg = '获取成功';
                    result.data = res;
                    callback(result);
                })
            }
        },
        {
            name: 'getonearticle',
            todo: (req, callback) => {
                let result = new ResultClass({ rel: false, msg: '操作失败！', code: '-1' });
                const url = req.url.split('?');
                const router = url[0].split('/');
                if (url.length == 2) {
                    const paras = url[1].split('&')[0].split('=')[1];
                    console.log(paras);
                    const client = InitRedis(redisConfig);
                    redisClient.getHashValue(client, 'article', paras).then((res) => {
                        result.rel = true;
                        result.msg = '获取成功';
                        result.data = res;
                        callback(result);
                    })
                }
                else {
                    result.msg = '未知ID';
                    callback(result);
                }
            }
        }
    ]
}


export class ResultClass {
    constructor(props) {
        this.rel = props.rel;
        this.msg = props.msg;
        this.data = props.data;
        this.code = props.code;
    }
}