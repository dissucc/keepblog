import render from './render';

export default function initSSR() {
    return async (req, res, next) => {
        if (req.url === "/favicon.ico") {
            next();
        }
        else {
            const result = await render(req, res, next);
            switch (result.code) {
                case 200:
                    return res.end(result.html);
                case 302:
                    return res.redirect(302, result.redirect);
                case 404:
                    next();
                    break;
                case 500:
                    next(result.error);
                    break;
                default:
                    next();
                    break;
            }
        }

    };
}