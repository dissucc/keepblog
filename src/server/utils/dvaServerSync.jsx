export default function sync(key, filter, memory) {
    return {
        onEffect: function (effect, { put }, model, actionType) {
            const temp = [];
            return function* (...args) { 
                if (filter(args[0])) {
                    memory.lock(key);
                }
        
                yield effect(...args);
                if (filter(args[0])) {
                    memory.release(key);
                }
            }
        }
    };
}