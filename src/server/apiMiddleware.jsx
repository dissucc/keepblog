import { request, getOptions } from './utils/request';

import ApiController from './apiController/ApiController';
import Article from './apiController/Article';
import Dome from './apiController/Dome';


export default function (req, res, next) {
    const controller = ApiController.getInstance();
    controller.use(Article);
    controller.use(Dome);
    controller.run(req, (data) => {
        if (data === false) {
            next();
        } else {
            res.send(data);
        }
    });

}

function doapi(url, options, callback) {
    request(url, options)
        .then(data => {
            callback(data);
        });
}