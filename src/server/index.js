require('babel-register');
const lessParser = require('postcss-less').parse;
require('css-modules-require-hook')({
    extensions: '.less',
    processorOpts: { parser: lessParser },
})

require('./server');
