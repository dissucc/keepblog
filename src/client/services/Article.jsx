import { request, getOptions, postOptions } from '../utils/request'

const actionName = 'article';

export function GetArticle(values) {
    return request(actionName + '/getarticle', getOptions(values));
}

export function AddArticle(values) {
    return request(actionName + '/addarticle', postOptions(values));
}

export function GetOneArticle(values) {
    return request(actionName + '/getonearticle', getOptions(values));
}

export default { GetArticle, AddArticle, GetOneArticle }