import React from 'react';
import classNames from 'classnames';
import './style/Banner.less';

export default class Banner extends React.Component {
    static defaultProps = {
        defaultCls: 'keep-banner',
    }
    constructor(props) {
        super(props);
    }
    render() {
        const {
            defaultCls, className, style
        } = this.props;
        const classes = classNames({
            className: className,
            [`${defaultCls}`]: defaultCls,
            [`${defaultCls}-selected`]: true
        })
        return (<div className={classes} style={style}>

        </div>)
    }


}