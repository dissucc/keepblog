import React from 'react';
import classNames from 'classnames';
import Jump from '../Jump';
import './style/Nav.less';

export default class Nav extends React.Component {
    static defaultProps = {
        defaultCls: 'keep-nav',
    }
    constructor(props) {
        super(props);
    }
    render() {
        const {
            defaultCls, className, style
        } = this.props;
        const classes = classNames({
            className: className,
            [`${defaultCls}`]: defaultCls,
            [`${defaultCls}-selected`]: true
        })
        return (<header className={classes} style={style}>
            <div className={`${defaultCls}-menu`}>
                <ul className={`${defaultCls}-menu-left`}>
                    <li><Jump path={'/'}>Blog</Jump></li>
                </ul>
                <ul className={`${defaultCls}-menu-right`}>
                    <li>Git</li>
                </ul>
            </div>
        </header>)
    }


}