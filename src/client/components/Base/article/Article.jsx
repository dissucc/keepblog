import React from 'react';
import classNames from 'classnames';
import './style/article.less';


export default class Article extends React.Component {
    static defaultProps = {
        defaultCls: 'keep-article',
    }
    constructor(props) {
        super(props);
    }
    render() {
        const {
            defaultCls, className, style, children, title, date
        } = this.props;
        const classes = classNames({
            className: className,
            [`${defaultCls}`]: defaultCls
        })
        return (
            <div style={style} className={classes}>
                <div className={`${defaultCls}-title`}>{title}
                    <div className={`${defaultCls}-time`}>
                        <i className="fa fa-calendar" aria-hidden="true"></i>
                        {date}</div>
                </div>
                <div className={`${defaultCls}-content`}> {children}</div>
            </div>
        )
    }


}