import React from 'react';
import PropTypes from 'prop-types'

class Jump extends React.Component {
    render() {
        const { path, sync, children, style, className } = this.props;
        return (
            <a href={path} onClick={() => {
                window.location.href = path;
            }} style={style} className={className} >{children}</a >
        );
    }

};
Jump.propTypes = {
    path: PropTypes.string,
    sync: PropTypes.bool
};
export default Jump;