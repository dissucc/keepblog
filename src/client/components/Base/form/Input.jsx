import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types'
import './style/Input.less';

class Input extends React.Component {
    static defaultProps = {
        defaultCls: 'keep-input',
        placeholder: ''
    }
    constructor(props) {
        super(props);
    }
    render() {
        const {
            defaultCls, className, style, title, placeholder, onChange
        } = this.props;
        const classes = classNames({
            className: className,
            [`${defaultCls}`]: defaultCls
        })
        return (<div>
            {!title || <label>{title}:</label>}
            <input placeholder={placeholder} className={classes} style={style}
                onChange={(value) => {
                    if (onChange) {
                        onChange(value.target.value);
                    }

                }} />
        </div>)
    }
}

Input.propTypes = {
    title: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func
};

export default Input;