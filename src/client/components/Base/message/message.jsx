import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import uid from 'uid';
import './style/message.less';

class message extends React.Component {
    //type:msg,loading
    static info(msg, duration = 3, callback) {
        this._getNotice(msg, 'info', duration);

    }

    static success(msg, duration = 3, callback) {
        this._getNotice(msg, 'check', duration);
    }

    static error(msg, duration = 3, callback) {
        this._getNotice(msg, 'times', duration);
    }

    static loading(msg, duration = 3, callback) {
        this._getNotice(msg, 'spinner', duration);
    }

    static _getNotice(msg, type, duration) {

        const ukey = uid(3);
        let div = document.getElementById('message');
        if (div === null) {
            div = document.createElement('div');
            div.className = 'keep-message-box';
            div.id = 'message';
        }

        const classes = classNames({
            'keep-message': true
        });
        document.body.appendChild(div);
        const showindex = setTimeout(() => {
            const old = document.getElementById('message' + ukey);
            if (old !== null) {
                if (div.children.length > 0) {
                    const oldClass = old.className + ' ' + 'keep-message-out';
                    old.className = oldClass;
                    const hideindex = setTimeout(() => {
                        ReactDOM.unmountComponentAtNode(div);
                        clearTimeout(hideindex);
                    }, 200)

                }
            }
            clearTimeout(showindex)
        }, duration * 1000);

        const faclasses = classNames({
            [`fa fa-${type}`]: type,
            [`fa-spin`]: type == 'spinner'
        })
        ReactDOM.render(
            <div className={classes} id={'message' + ukey}>
                <div className={'keep-message-notice'}>
                    <div className={'keep-message-notice-content'}>
                        <i className={faclasses}></i> {msg}</div>
                </div>
            </div>
            , div)
    }
    render() {
        return null;
    }
}


export default message;