import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types'
import './style/button.less';


class Button extends React.Component {
    static defaultProps = {
        defalutCls: 'keep-btn',
        loading: false,
        type: 'default',
        size: 'xm'
    }
    constructor(props) {
        super(props);
    }
    render() {
        const {
            defalutCls, className, size, type, onClick, text, style
        } = this.props;
        const classes = classNames({
            className: className,
            [`${defalutCls}`]: defalutCls,
            [`${defalutCls}-${size}`]: size,
            [`${defalutCls}-${type}`]: type
        })
        return <button className={classes} style={style} type='button' onClick={onClick}  >{text}</button>
    }
}

Button.propTypes = {
    size: PropTypes.string,
    text: PropTypes.any,
    type: PropTypes.string,
    onClick: PropTypes.func
};

export default Button;