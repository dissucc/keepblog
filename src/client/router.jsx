import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import {
    BrowserRouter as Router
} from 'react-router-dom';

import Index from './routes/Index';
import ArticleDetail from './routes/ArticleDetail';
import AddArticle from './routes/AddArticle';
import { Nav } from './components/index';

export const routes = [
    { path: '/', exact: true, component: Index },
    { path: '/detail/:id',  component: ArticleDetail },
    { path: '/add', sync: true, component: AddArticle }
]

export function getRouters(isServer) {
    return (
        <div>
            <Nav />
            {routes.map((item, index) => {
                return (<FadingRoute key={index} isServer={isServer} {...item} />);
            })}
        </div>
    )
}
const FadingRoute = ({ component: Component, ...rest }) => (
    <div>

        <Route {...rest} render={props => (
            <Component {...props} isServer={rest.isServer} />
        )} />
    </div>
)