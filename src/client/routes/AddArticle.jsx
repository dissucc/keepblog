import React from 'react';
import { connect } from 'dva';
import { Input, Button } from '../components/index';

import '../style/AddArticle.less';
let editor = null;
class AddArticle extends React.Component {
    static defaultProps = {
        defaultCls: 'keep-article-add',
    }
    state = {
        editorContent: '',
        title: ''
    }
    componentDidMount() {
        var E = require('wangeditor');
        const elem = this.refs.editorElem
        editor = new E(elem)
        // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
        editor.customConfig.onchange = html => {
            this.setState({
                editorContent: html
            })
        }
        editor.create()
    }
    addArticle = () => {
        const { editorContent, title } = this.state;
        const { dispatch } = this.props;
        dispatch({ type: 'article/addArticle', payload: this.state });

    }
    render() {
        const { defaultCls } = this.props;
        return (
            <div className={'keep-box'}>
                <div className={`${defaultCls}-title`}>
                    <h2> 添加文章</h2>
                </div>

                <div className={`${defaultCls}-form`}>
                    <Input title={'文章标题'} placeholder={'请输入文章标题'} onChange={(value) => {
                        this.setState({ title: value })
                    }} />
                </div>
                <div className={`${defaultCls}-form`}>
                    <label>文章内容:</label>
                    <div ref="editorElem" style={{ textAlign: 'left', margin: '10px 10px' }}>
                    </div>
                </div>
                <div className={`${defaultCls}-form`}>
                    <Button style={{ float: 'right', marginRight: '10px' }} text={'提交'} onClick={this.addArticle} />
                </div>
            </div >
        )
    }
}

export default connect(({ article }) => ({ article }))(AddArticle);