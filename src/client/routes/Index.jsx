import React from 'react';
import { connect } from 'dva';
import { Jump, Banner, Article, message, Button } from '../components/index';

class Index extends React.Component {
    componentWillMount() {
        const { isServer } = this.props;
        if (isServer) {
            this.props.dispatch({ type: 'article/getArticle', payload: { guid: undefined } });
        }
    }
    render() {
        const { resultClass } = this.props.article;
        return (
            <div style={{ width: '100%', maxWidth: '1000px', margin: '0 auto' }}>
                {!resultClass || Object.keys(resultClass.data).map((item, index) => {
                    const tmpldata = JSON.parse(resultClass.data[item]);
                    return (<Jump key={'art' + item} path={'/detail/' + item}><Article title={tmpldata.title} date={'2018-03-01'}>点击查看</Article></Jump>)
                })}

                <Button text='info' onClick={() => {
                    message.info('测试', 3);
                }} />
                <Button text='check' onClick={() => {
                    message.success('测试', 3);
                }} />
                <Button text='times' onClick={() => {
                    message.error('测试', 3);
                }} />

                <Button text='loading' onClick={() => {
                    message.loading('测试', 3);
                }} />
            </div >
        )
    }
}

export default connect(({ article }) => ({ article }))(Index);