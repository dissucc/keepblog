import React from 'react';
import { connect } from 'dva';
import '../style/ArticleDetail.less';

class AriticleDetail extends React.Component {
    static defaultProps = {
        defaultCls: 'keep-article-detail',
    }
    componentWillMount() {
        const { isServer } = this.props;

        if (isServer) {
            console.log(this.props.match.params.id)
            this.props.dispatch({ type: 'article/getOneArticle', payload: { id: this.props.match.params.id } });
        }
    }
    render() {
        const { defaultCls } = this.props;
        const { resultClass } = this.props.article;
        let art = { title: '未找到该文章', editorContent: '' };

        if (resultClass) {
            art = JSON.parse(resultClass.data);
        }
        return (
            <div>
                <div className={`${defaultCls}-title`}>
                    <h2> {art.title}</h2>
                </div>
                <div className={`${defaultCls}-content`}>
                    <div className={`${defaultCls}-info`}>
                        <ul>
                            <li><i className="fa fa-calendar" aria-hidden="true"></i>2018-02-01</li>
                        </ul>
                    </div>
                    <div className={`${defaultCls}-article`} dangerouslySetInnerHTML={{ __html: art.editorContent }} >
                    </div>

                    <div className={`${defaultCls}-footer`}>
                        <div className={`${defaultCls}-footer-left`}>上一篇：xxxxx</div>
                        <div className={`${defaultCls}-footer-right`}>下一篇：xxxxx</div>
                        <div style={{ clear: 'both' }} />
                    </div>
                </div>
            </div >
        )
    }
}

export default connect(({ article }) => ({ article }))(AriticleDetail);