
import { GetArticle, AddArticle, GetOneArticle } from '../services/Article';

const baseName = 'article';
export default {

    namespace: baseName,

    state: {
        postloading: false,
        show: '',
        resultClass: undefined

    },
    effects: {
        *addArticle({ payload }, { call, put }) {
            yield put({ type: 'showloading' });
            const result = yield call(AddArticle, payload);
            yield put({ type: 'hideloading' });
            const { data } = result;
            if (data.rel) {
                window.location.href = '/';
            }
            else {

            }
        },
        *getArticle({ payload }, { call, put }) {
            const result = yield call(GetArticle, payload);
            const { data } = result;
            yield put({ type: 'setdata', payload: data });
        },

        *getOneArticle({ payload }, { call, put }) {
            const result = yield call(GetOneArticle, payload);
            const { data } = result;
            yield put({ type: 'setdata', payload: data });
        }
    },

    reducers: {
        showloading(state) {
            return { ...state, postloading: true };
        },
        hideloading(state) {
            return { ...state, postloading: false };
        },

        setdata(state, { payload }) {
            return { ...state, resultClass: payload };
        }

    },

};

