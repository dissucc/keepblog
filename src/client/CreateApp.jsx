import React from 'react';
import dva from 'dva';
import createLoading from 'dva-loading';
import Article from './model/Article';
import { deepClone } from '../server/utils/helper';
import {
    BrowserRouter,
    MemoryRouter
} from 'react-router-dom';
import { getRouters } from './router';

export default function CreateApp(opts, isServer) {
    var nowOption = ({
        ...createLoading({
            effects: true,
        }),
        history: opts.history,
        initialState: opts.initialState,
    });
    const app = dva(nowOption);
    if (!isServer) {
        app.router(({ history }) => {
            return (
                <BrowserRouter>
                    {getRouters(isServer)}
                </BrowserRouter>
            );
        });
    }
    app.model(deepClone(Article));
    return app;
}