


import React from 'react';
import CreateApp from '../client/CreateApp';
import { createBrowserHistory } from 'history';
import "babel-polyfill";
const app = CreateApp({
    history: createBrowserHistory(),
    initialState: window.__INITIAL_STATE__
}, false);

app.start('#app');
