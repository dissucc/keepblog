

const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
//const { buildPlugins } = require('./src/keep/config/plugins')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = {

    // 我们应用的入口, 在 `src` 目录 (我们添加到下面的 resolve.modules):
    entry: {
        index: './src/startup/index.jsx',
        vendor: ['react', 'react-dom', 'react-router', 'react-router-dom']
    },

    // 配置 devServer 的输出目录和 publicPath
    output: {
        filename: '[name].js',
        path: path.join(__dirname, './src/static'),
        publicPath: '/static/',
        chunkFilename: "[name].js",
    },

    // 配置 devServer 
    devServer: {
        port: 3000,
        historyApiFallback: true,
        inline: false,
        stats: {
            modules: false
        }
    },


    resolve: {
        // 首先寻找模块中的 .js(x) 文件
        extensions: [' ', '.jsx', '.js', '.css', '.less', '.jpg', '.png'],

        // 在模块中添加 src, 当你导入文件时，可以将 src 作为相关路径
        modules: ['src', 'node_modules'],
    },

    module: {
        loaders: [
            // .ts(x) 文件应该首先经过 Typescript loader 的处理, 然后是 babel 的处理
            { test: /\.jsx?$/, loaders: ['babel-loader'], exclude: /node_modules/, include: path.resolve('src') },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader"]
                })
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader", "less-loader"]
                })
            },

            {
                test: /\.(png|jpg)$/,
                use: ExtractTextPlugin.extract({
                    fallback: "file-loader",
                    use: ["url-loader"]
                })

            }

        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            output: {
                comments: false,
            },
            compress: {
                warnings: false
            }
        }),
        new webpack.HotModuleReplacementPlugin(),//热更
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor'
        }),//第三方库内容打包
        // new CleanWebpackPlugin('/static/'),//清理打包文件夹
        new ExtractTextPlugin("[name].css", {
            disable: false,
            allChunks: true,
        }),//样式文件配置
        //  new HtmlWebpackPlugin({
        // template: './src/client/index.html',
        //   hash: false,
        //  }),//模板文件配置
        new CopyWebpackPlugin([
            { from: './src/startup/sw.js', to: './' }
        ])
    ],
}
