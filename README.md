# KEEPBLOG

示例地址：[点我](http://blog.hourxu.com)

真正的JavaScript项目，不需其他语言，博客文章保存在redis中

同构项目，一份源码，即可完成项目编写

该项目中，你将学到React最新版本的全家桶

适配移动端

基于： 

      React@16.2

      Dva@2.*

      React-Router@4.2，

      Node

      .......



#项目初始化流程


```
cnpm i
```

修改config.js文件
![输入图片说明](https://gitee.com/uploads/images/2018/0306/194054_ffaa688c_332899.png "屏幕截图.png")

如果redis在本地，则不需要配置redisConfig

Config.address需要配置和node启动同样的地址，如果发布到服务器上，则配置最后的域名地址

然后

```
npm run dev
```

启动后：输入网址：http://localhost:7070/add,

填写你的第一篇文章

![输入图片说明](https://gitee.com/uploads/images/2018/0306/194343_ed562914_332899.png "屏幕截图.png")

然后回到首页：

![输入图片说明](https://gitee.com/uploads/images/2018/0306/194405_9de87654_332899.png "屏幕截图.png")

OK，成功！！！

 :cry: 需要服务端渲染的页面，请使用Jump组件跳转(参考Index)

#开发计划

 :white_check_mark: 文章展示

 :white_check_mark: 添加文章

 :white_large_square: 增加文章分类

 :white_large_square: 增加文章标签

 :white_large_square: 增加文章评论

 :white_large_square: 增加文章添加后台，并可以删除文章

 :white_large_square: 增加页面上的动画效果

 more...............


